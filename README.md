Multi Redmine on Docker
====

複数のRedmineをDockerでホストするdocker-composeです。

## Description
リバースプロキシ(Nginx)でサブディレクトリにリダイレクトさせます。

## Requirement
* Docker
* docker-compose

## Install
```
$ docker network create -d bridge public
$ git clone https://gitlab.com/fiolab/multi-redmine-on-docker.git
$ cd multi-redmine-on-docker
$ docker-compose -f ./first-redmine/docker-compose.yml up -d
$ docker-compose -f ./second-redmine/docker-compose.yml up -d
$ docker-compose -f ./reverse-proxy/docker-compose.yml up -d
$ docker ps
CONTAINER ID        IMAGE                       COMMAND                  CREATED             STATUS              PORTS                NAMES
20352b2598d9        nginx:1.13.10               "nginx -g 'daemon of…"   4 minutes ago       Up 3 minutes        0.0.0.0:80->80/tcp   reverse-proxy
cda3f5e7c970        sameersbn/redmine:3.4.4-2   "/sbin/entrypoint.sh…"   6 minutes ago       Up 6 minutes        80/tcp, 443/tcp      second-redmine-ap
afbafb81b927        sameersbn/mysql:5.7.22-1    "/sbin/entrypoint.sh…"   6 minutes ago       Up 6 minutes        3306/tcp             second-redmine-db
ef21950c208e        sameersbn/redmine:3.4.4-2   "/sbin/entrypoint.sh…"   15 minutes ago      Up 7 minutes        80/tcp, 443/tcp      first-redmine-ap
a4703be81324        sameersbn/mysql:5.7.22-1    "/sbin/entrypoint.sh…"   15 minutes ago      Up 7 minutes        3306/tcp             first-redmine-db
```

## Usage
Dockerホストから以下URLでアクセス可能。
* http://localhost/first-redmine
* http://localhost/second-redmine

Redmine初期アカウント
* user: admin
* pass: admin